provider "aws" {
  region = "us-east-1"
}


data "aws_vpc" "existing_vpc" {
  id = "vpc-056c047fbf6d19b1d"
}


data "aws_subnet" "existing_subnet" {
  vpc_id = data.aws_vpc.existing_vpc.id
  id     = "subnet-033957339019b1d1e"
}

data "aws_security_group" "existing_security_group" {
  id = "sg-0e25ee7156c94a52f"


}

resource "aws_instance" "puppetMaster" {
  ami                         = "ami-07d9b9ddc6cd8dd30"
  instance_type               = "t2.medium"
  subnet_id                   = data.aws_subnet.existing_subnet.id
  key_name                    = "ec2key"
  associate_public_ip_address = true
  security_groups             = [data.aws_security_group.existing_security_group.id] // Attach security group to instance

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("d:\\Users\\User\\Downloads\\ec2key.pem")
    host        = aws_instance.puppetMaster.public_ip
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "wget https://apt.puppetlabs.com/puppet8-release-jammy.deb",
      "sudo dpkg -i puppet8-release-jammy.deb",
      "sudo apt update -y",
      "sudo apt install -y puppetserver",
      "sudo sed -i -e 's/-Xms2g -Xmx2g / -Xms250m -Xmx250m /g' /etc/default/puppetserver",
      "sudo systemctl start puppetserver"
    ]
  }

  tags = {
    Name = "puppetMaster"
  }
}